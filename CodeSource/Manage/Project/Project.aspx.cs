﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;

namespace CodeSource.Manage.Project
{
    public partial class Project : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();
        ProjectManager projectManager = new ProjectManager();
        int projectId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (accountManager.ActiveUser == null)
                Response.Redirect("/LogReg.aspx");

            if (String.IsNullOrWhiteSpace(Request["id"]))
            {
                Button_Create.Visible = true;
                Button_Save.Visible = false;
                Button_Delete.Visible = false;

                PlaceHolder_Create.Visible = true;
            }
            else
            {
                Button_Create.Visible = false;
                Button_Save.Visible = true;
                Button_Delete.Visible = true;

                PlaceHolder_Create.Visible = false;

                projectId = Int32.Parse(Request["id"]);

                //Make sure to not reload project information when they click something like save.
                //It'd basically result in them never being able to save because this restores data before every save.
                if (!IsPostBack)
                {
                    //Just checks if user is owner, and if so, allows editing.
                    //Separated from above for clarity.
                    if (projectManager.GetProjectByID(projectId).Owner == accountManager.ActiveUser.ID)
                    {
                        TextBox_Name.Text = projectManager.GetProjectByID(projectId).Name;
                        TextBox_Desc.Text = projectManager.GetProjectByID(projectId).Description;
                        TextBox_Language.Text = projectManager.GetProjectByID(projectId).Languages;
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }

            LoadProjects();
        }

        private void LoadProjects()
        {
            AccountManager ac = new AccountManager(); //We need this in order get a current version of ProjectOwned. The existing one won't show new projects.
            Literal_ExistingProjects.Text = "";
            foreach (var project in ac.ActiveUser.ProjectsOwned)
            {
                Literal_ExistingProjects.Text += "<tr><td>" + "<a href=\"/Project.aspx?id=" + project.ID + "\">" + project.Name + "</a>" + "</td><td>" + project.Description + "</td><td>" + "<a href=\"/Manage/Project/Project.aspx?id=" + project.ID + "\">Manage</a>" + "</td></tr>";
            }
        }

        protected void Button_Create_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(TextBox_Name.Text) || String.IsNullOrWhiteSpace(TextBox_Desc.Text) || String.IsNullOrWhiteSpace(TextBox_Language.Text))
                {
                    ShowAlert("error", "You can not leave any fields empty.");
                }
                else
                {
                    projectManager.CreateProject(accountManager.ActiveUser, TextBox_Name.Text, TextBox_Desc.Text, TextBox_Language.Text);
                    LoadProjects();
                    ShowAlert("success", "You have successfully created a project.");
                }
            }
            catch
            {
                ShowAlert("error", "An unknown error has occured. Please review your input.");
            }
        }

        protected void Button_CreateBitbucket_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(TextBox_BitUser.Text) || String.IsNullOrWhiteSpace(TextBox_BitRepo.Text))
                {
                    ShowAlert("error", "You can not leave any fields empty.");
                }
                else
                {
                    projectManager.CreateProjectFromBitbucket(accountManager.ActiveUser, TextBox_BitUser.Text, TextBox_BitRepo.Text, new AnonymousAuthentication());
                    LoadProjects();
                    ShowAlert("success", "You have successfully imported a project from Bitbucket.");
                }
            }
            catch
            {
                ShowAlert("error", "An unknown error has occured. Please review your input.");
            }
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            try
            {
                projectManager.GetProjectByID(projectId).Name = TextBox_Name.Text;
                projectManager.GetProjectByID(projectId).Description = TextBox_Desc.Text;
                projectManager.GetProjectByID(projectId).Languages = TextBox_Language.Text;
                projectManager.SubmitChanges();
                ShowAlert("success", "You have successfully updated your project.");
            }
            catch
            {
                ShowAlert("error", "An unknown error has occured. Please review your input.");
            }
        }

        protected void Button_Delete_Click(object sender, EventArgs e)
        {
            projectManager.DeleteProject(projectManager.GetProjectByID(projectId));
            Response.Redirect("/Manage/Project/Project.aspx");
        }

        protected void ShowAlert(string type, string message)
        {
            if (type == "success")
            {
                Div_Alert.Attributes["class"] = "alert alert-success";
            }
            else
            {
                Div_Alert.Attributes["class"] = "alert alert-error";
            }

            Div_Alert.InnerText = message;

            Div_Alert.Visible = true;
        }
    }
}