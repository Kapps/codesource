﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Project.aspx.cs" Inherits="CodeSource.Manage.Project.Project" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            setFields();

            $('#DropDownList_ProjType').change(function () {
                setFields();
            });

            function setFields() {
                if ($('#DropDownList_ProjType').val() == 'CodeSource') {
                    $('.t2').hide();
                    $('.t1').show();
                } else {
                    $('.t1').hide();
                    $('.t2').show();
                }
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="row">
        <div class="span3">
            <div class="well">
                <div class="control-group">
                    <label class="control-label">Project Type:</label>
                    <div class="controls">
                        <!--No real reason to make it an asp.net control other than consistency.-->
                        <asp:DropDownList ID="DropDownList_ProjType" runat="server" ClientIDMode="Static" CssClass="input-block-level">
                            <asp:ListItem>CodeSource</asp:ListItem>
                            <asp:ListItem>Bitbucket</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group t1">
                    <label class="control-label">Project Name:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_Name" ClientIDMode="static" runat="server" CssClass="input-block-level"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group t1">
                    <label class="control-label">Language(s) (Comma Separated):</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_Language" ClientIDMode="static" runat="server" CssClass="input-block-level"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group t1">
                    <label class="control-label">Project Description:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_Desc" ClientIDMode="static" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group t2">
                    <label class="control-label">Bitbucket User ID:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_BitUser" ClientIDMode="static" runat="server" CssClass="input-block-level"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group t2">
                    <label class="control-label">Bitbucket Repository Name:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_BitRepo" ClientIDMode="static" runat="server" CssClass="input-block-level"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:PlaceHolder ID="PlaceHolder_Create" runat="server">
                            <asp:Button ID="Button_Create" runat="server" Text="Create" CssClass="btn btn-large btn-primary t1" OnClick="Button_Create_Click" />
                            <asp:Button ID="Button_CreateBitbucket" runat="server" Text="Create" CssClass="btn btn-large btn-primary t2" OnClick="Button_CreateBitbucket_Click" />
                        </asp:PlaceHolder>
                        <asp:Button ID="Button_Save" runat="server" Text="Save" CssClass="btn btn-large btn-primary" OnClick="Button_Save_Click" />
                        <asp:Button ID="Button_Delete" runat="server" Text="Delete" CssClass="btn btn-large btn-primary" OnClick="Button_Delete_Click" />
                    </div>
                </div>
                <div id="Div_Alert" runat="server" visible="false"></div>
            </div>
        </div>
        <div class="span9"><!--This span just fits to content.-->
            <div class="well">
            <h2>Existing Projects</h2>
                <table class="table">
                    <tr>
                        <th>Project Name</th><th>Description</th><th>Action</th>
                    </tr>
                    <asp:Literal ID="Literal_ExistingProjects" runat="server"></asp:Literal>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
