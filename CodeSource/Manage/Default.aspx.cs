﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource.Manage
{
    public partial class Default : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (accountManager.ActiveUser == null)
                Response.Redirect("/LogReg.aspx");
        }
    }
}