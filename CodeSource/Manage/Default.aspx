﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CodeSource.Manage.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="span12">
        <div class="row">
            <h4>User Management</h4>
            <asp:HyperLink ID="HyperLink_EditProfile" runat="server" NavigateUrl="/Manage/User/User.aspx">Edit Profile</asp:HyperLink>
            <br />
            <asp:HyperLink ID="HyperLink_ChangePassword" runat="server" NavigateUrl="/Manage/User/ChangePassword.aspx">Change Password</asp:HyperLink>
            <br />
            <asp:HyperLink ID="HyperLink_SetAvatar" runat="server" NavigateUrl="https://en.gravatar.com/">Set Avatar</asp:HyperLink>
            <br />
            <asp:HyperLink ID="HyperLink_ConnectFacebook" runat="server" NavigateUrl="/Manage/User/ConnectFacebook.aspx">Connect Facebook</asp:HyperLink>
        </div>
        <div class="row">
            <h4>Project Management</h4>
            <asp:HyperLink ID="HyperLink_CreateProject" runat="server" NavigateUrl="/Manage/Project/Project.aspx">Create/Edit/Delete a Project</asp:HyperLink>
        </div>
    </div>
</asp:Content>
