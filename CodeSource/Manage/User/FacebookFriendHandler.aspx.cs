﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;

namespace CodeSource.Manage.User
{
    public partial class FacebookFriendHandler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = Context;
            context.Response.Clear();

            String info = context.Request.Form[0].ToString();
            String[] fbIDs = info.Split(',');

            String result = "<h3>Facebook Friends</h3><center><table>";

            AccountManager accountManager = new AccountManager();

            foreach (var Account in accountManager.GetUsersWithLinkedIdentifier(AuthNetwork.Facebook, fbIDs))
            {
                result = result + "<tr><td>" + Account.FirstName + " " + Account.LastName + "</td><td><a href='#' onclick='addFriend(" + Account.ID + ")'>Add Friend</a></td></tr>";
            }
            result = result + "</center></table>";

            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
            context.Response.End();
            return;

        }
    }
}