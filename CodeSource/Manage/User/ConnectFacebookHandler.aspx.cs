﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource.Manage.User
{
    public partial class ConnectFacebookHandler1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = Context;
            context.Response.Clear();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, string> json = jss.Deserialize<Dictionary<string, string>>(context.Request.Form[0].ToString());

            int facebookId = Int32.Parse(json["facebookid"]);
            String facebookName = json["facebookName"];
            context.Response.ContentType = "text/plain";

            using (var Accounts = new AccountManager())
            {
                var Account = Accounts.ActiveUser;
                //see if user is logged in
                if (Account == null)
                {
                    //give error message
                    String response = "You must be logged in to CodeSource to connect to Facebook";
                    context.Response.Write(response);
                    context.Response.End();
                    return;
                }
                //see if user already has connected to facebook
                else if (Accounts.GetNetworkLink(Account, AuthNetwork.Facebook) != null)
                {
                }
                //else add facebook infos
                else
                {
                    Accounts.LinkAccountToNetwork(AuthNetwork.Facebook, Account, facebookId.ToString(), facebookName);
                }
                string respons = "Connected";
                context.Response.Write(respons);
                context.Response.End();
                return;
            }
        }
    }
}