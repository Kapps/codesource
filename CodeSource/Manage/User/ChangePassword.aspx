﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="CodeSource.Manage.User.ChangePass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="row">
        <div class="span3">
            <div class="well well-fix">
                <div class="control-group">
                    <label class="control-label">Old Password:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_OldPass" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">New Password:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_NewPass" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Confirm New Password:</label>
                    <div class="controls">
                        <asp:TextBox ID="TextBox_NewPassConf" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:Button ID="Button_Save" runat="server" Text="Save"  CssClass="btn btn-large btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
