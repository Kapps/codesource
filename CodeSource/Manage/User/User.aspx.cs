﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;

namespace CodeSource.Management
{
    public partial class User : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (accountManager.ActiveUser == null)
                Response.Redirect("/LogReg.aspx");

            if (!IsPostBack)
            {
                TextBox_FirstName.Text = accountManager.ActiveUser.FirstName;
                TextBox_LastName.Text = accountManager.ActiveUser.LastName;
                TextBox_Email.Text = accountManager.ActiveUser.Email;
                TextBox_Description.Text = accountManager.ActiveUser.Description;
                TextBox_Birthday.Text = (accountManager.ActiveUser.Birthdate != null) ? ((DateTime)accountManager.ActiveUser.Birthdate).ToShortDateString() : "";
            }
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            try
            {
                AccountManager accountManagerTemp = new AccountManager();
                string errorDetails;
                accountManagerTemp.AttemptAuthenticate(accountManager.ActiveUser.Username, TextBox_Password.Text, out errorDetails);

                if (errorDetails == "Login Successful")
                {
                    DateTime birthdayParsed;
                    bool birthdayParsable = DateTime.TryParse(TextBox_Birthday.Text, out birthdayParsed);

                    accountManager.ActiveUser.FirstName = TextBox_FirstName.Text;
                    accountManager.ActiveUser.LastName = TextBox_LastName.Text;
                    accountManager.ActiveUser.Email = TextBox_Email.Text;
                    accountManager.ActiveUser.Description = TextBox_Description.Text;

                    if (birthdayParsable == true)
                    {
                        accountManager.ActiveUser.Birthdate = birthdayParsed;
                        accountManager.SubmitChanges();
                        ShowAlert("success", "You have successfully updated your profile.");
                    }
                    else if (birthdayParsable == false && String.IsNullOrWhiteSpace(TextBox_Birthday.Text)) //Birthday was left empty.
                    {
                        accountManager.ActiveUser.Birthdate = null;
                        accountManager.SubmitChanges();
                        ShowAlert("success", "You have successfully updated your profile.");
                    }
                    else //Birthday is just unparseable and has stuff in it.
                    {
                        ShowAlert("error", "Your birthday could not be interpreted as valid.");
                    }
                }
                else
                {
                    ShowAlert("error", "Password confirmation failed.");
                }
            }
            catch
            {
                ShowAlert("error", "An unknown error has occured. Please review your input.");
            }
        }

        protected void ShowAlert(string type, string message)
        {
            if (type == "success")
            {
                Div_Alert.Attributes["class"] = "alert alert-success";
            }
            else
            {
                Div_Alert.Attributes["class"] = "alert alert-error";
            }

            Div_Alert.InnerText = message;

            Div_Alert.Visible = true;
        }
    }
}