﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConnectFacebook.aspx.cs" Inherits="CodeSource.Manage.User.ConnectFacebook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
    <script src="http://connect.facebook.net/en_US/all.js"></script>
    <script>
        var name;
        var id;

        FB.init({ 
            appId:'382642158482669', 
            cookie:true, 
            status:true, 
            xfbml:true,
            oauth:true
        });

        function showLoginButton() {
            document.getElementById('fb-root').innerHTML = (
              '<img onclick="showLogin()" style="cursor: pointer;"' + 'src="http://www.bebo.com/img/facebook-login.png">'
            );
        }

        function showLogin() {
            //FB.login();
            FB.login(function (response) {
                if (response.authResponse) {
                    FB.api('/me', function (response) {
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, { scope: 'email' });
        }

        function onStatus(response) {
            if (response.status === 'connected') {
                getAccountInfo();
            } else {
                showLoginButton();
            }
        }
        FB.getLoginStatus(function (response) {
            onStatus(response);
            FB.Event.subscribe('auth.statusChange', onStatus);
        });

        function getAccountInfo() {
            id = FB.getUserID();
            $('#fb-root').hide();
            FB.api({
                    method: 'fql.query',
                    query: 'SELECT name FROM user WHERE uid=' + FB.getUserID()
                },
                function (response) {
                    name = response[0].name;
                    facebookConnect();
                }
            );
        }

        function facebookConnect() {
            var json = "{ \"request\": \"facebookinfo\", \"facebookid\":\"" + id + "\", \"facebookName\":\"" + name + "\"}";
            $.ajax({
                type: 'POST',
                url: "ConnectFacebookHandler.aspx",
                data: json,
                success: function (data) {
                    if (data.toString() != "Connected") {
                        $('#friendsTable').html("<center>" + data + "</center>");
                    }
                    else {
                        $('#friendsTable').html("<center>" + data + "</center>");
                        getFriends();
                    }
                },
                error: function (data, status, jqXHR) { alert("ERROR:" + data + "|" + status + "|" + jqXHR) },
            });
        };

        function getFriends() {
            var friendID = "";
            FB.api('/me/friends', function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    friendID = friendID + response.data[i].id;
                    if ((i + 1) < response.data.length) friendID = friendID + ",";
                }

                $.ajax({
                    type: 'POST',
                    url: "FacebookFriendHandler.aspx",
                    data: friendID,
                    success: function (data) {
                        $('#friendsTable').html(data);
                    },
                    error: function (data, status, jqXHR) { alert("ERROR:" + data + "|" + status + "|" + jqXHR) },
                });
            });
        }

        function addFriend(id) {
            $.ajax({
                type: 'POST',
                url: "addFriendHandler.aspx",
                data: (id + " "),
                success: function (data) {
                    alert(data);
                },
                error: function (data, status, jqXHR) { alert("ERROR:" + data + "|" + status + "|" + jqXHR) },
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    	  <div id="fb-root"></div>
    <center>
    <p id="facebookIntro">By logging into facebook we are able to help you find your facebook friends that are also here on CodeSource</p>
    </center>
    <div id="friendsTable">

    </div>
</asp:Content>
