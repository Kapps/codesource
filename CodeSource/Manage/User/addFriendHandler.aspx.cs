﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;

namespace CodeSource.Manage.User
{
    public partial class addFriendHandler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = Context;
            context.Response.Clear();

            String info = context.Request.Form[0].ToString();
            int friendID = Int32.Parse(info);
            
            AccountManager manager = new AccountManager();
            Account current = manager.ActiveUser;
            Account friend = manager.GetAccountByID(friendID);
            manager.AddFriend(current, friend);

            context.Response.ContentType = "text/plain";
            context.Response.Write("response from addFriendHandler");
            context.Response.End();
            return;

        }
    }
}