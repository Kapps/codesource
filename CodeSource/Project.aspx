﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Project.aspx.cs" Inherits="CodeSource.Project" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="row">
        <div class="span6">
            <div class="row">
                <div class="span6">
                    <h2><asp:Label ID="Label_Name" runat="server" Text="Label"></asp:Label></h2>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <asp:Button ID="Button_Follow" runat="server" Text="Follow/Unfollow Project" CssClass="btn" OnClick="Button_Follow_Click" />
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Description</h4>
                    <asp:Literal ID="Literal_Description" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Info</h4>
                    Owner: 
                    <asp:HyperLink ID="HyperLink_Owner" runat="server">HyperLink</asp:HyperLink>
                    <br />
                    Language(s): 
                    <asp:Label ID="Label_Language" runat="server" Text="Label"></asp:Label>
                    <br />
                    Size: 
                    <asp:Label ID="Label_Size" runat="server" Text="Label"></asp:Label>
                    <br />
                    External Link: 
                    <asp:HyperLink ID="HyperLink_ExtLink" runat="server">HyperLink</asp:HyperLink>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Commits</h4>
                    <asp:Literal ID="Literal_Commits" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Files</h4>
                    Commit Date: <asp:Label ID="Label_CommitDate" runat="server" Text="Label"></asp:Label>
                    <br />
                    <asp:Literal ID="Literal_Files" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Similar Projects</h4>
                    <asp:Literal ID="Literal_SimilarProjects" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Followers</h4>
                    <asp:Literal ID="Literal_Followers" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class="span6">
                    <h4>Comments</h4>
                    <asp:Literal ID="Literal_Comments" runat="server"></asp:Literal>
                    <br />
                    <asp:TextBox ID="TextBox_Comment" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <br />
                    <asp:Button ID="Button_AddComment" runat="server" Text="Submit" CssClass="btn" OnClick="Button_AddComment_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
