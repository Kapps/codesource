﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;
using System.Text;

namespace CodeSource
{
    public partial class User : System.Web.UI.Page
    {
        struct StreamItem
        {
            public DateTime dateTime { get; set; }
            public string content { get; set; }
        }

        AccountManager accountManager = new AccountManager();
        CommentManager commentManager = new CommentManager();
        ProjectManager projectManager = new ProjectManager();
        Account requestedAccount = new Account();
        int userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Request["id"]))
                Response.Redirect("/Default.aspx");
            else
                userId = Int32.Parse(Request["id"]);

            requestedAccount = accountManager.GetAccountByID(userId);

            if (!IsPostBack)
            {
                LoadAvatar();
                LoadInfo();
                SetFriendButton();
                LoadComments();
                SetCommentButton();
                SetCommentTextbox();
                LoadDescription();
                LoadFriends();
                LoadProjects();
                LoadStream();
            }
        }

        private void LoadAvatar()
        {
            Image_Avatar.ImageUrl = requestedAccount.GetGravatarUrl(GravatarDefaultStyle.Wavatar) + "&s=200";
        }

        private void LoadInfo()
        {
            Label_FullName.Text = requestedAccount.FirstName + " " + requestedAccount.LastName;
            Label_Username.Text = "(" + requestedAccount.Username + ")";
            Label_Birthday.Text = requestedAccount.Birthdate == null ? "N/A" : ((DateTime)requestedAccount.Birthdate).ToLongDateString();
            HyperLink_Email.Text = requestedAccount.Email;
            HyperLink_Email.NavigateUrl = "mailto:" + requestedAccount.Email;
        }

        protected void Button_Friend_Click(object sender, EventArgs e)
        {
            if (accountManager.IsUserFriendsWith(accountManager.ActiveUser, requestedAccount))
                accountManager.RemoveFriend(accountManager.ActiveUser, accountManager.GetAccountByID(userId));
            else
                accountManager.AddFriend(accountManager.ActiveUser, accountManager.GetAccountByID(userId));

            accountManager.SubmitChanges();

            SetFriendButton();
        }

        protected void SetFriendButton()
        {
            if (accountManager.ActiveUser != null)
            {
                if (accountManager.IsUserFriendsWith(accountManager.ActiveUser, requestedAccount))
                    Button_Friend.Text = "Remove Friend";
                else
                    Button_Friend.Text = "Add Friend";
            }
            else
            {
                Button_Friend.Visible = false;
            }
        }

        protected void Button_AddComment_Click(object sender, EventArgs e)
        {
            commentManager.CreateComment(requestedAccount, accountManager.ActiveUser, TextBox_Comment.Text);
            LoadComments();
        }

        protected void LoadComments()
        {
            if (commentManager.GetCommentsOnUser(requestedAccount).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var comment in commentManager.GetCommentsOnUser(requestedAccount).OrderByDescending(c => c.Created))
                {
                    sb.Append("<b>" + comment.Created.ToShortDateString() + " " + comment.Created.ToString("hh:mm tt") + "</b> - " + "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a>: " + comment.Contents + "<br />");
                }
                Literal_Comments.Text = sb.ToString();
            }
            else
            {
                Literal_Comments.Text = "N/A<br />";
            }
        }

        protected void SetCommentButton()
        {
            if (accountManager.ActiveUser == null)
                Button_AddComment.Visible = false;
        }

        protected void SetCommentTextbox()
        {
            if (accountManager.ActiveUser == null)
                TextBox_Comment.Visible = false;
        }

        protected void LoadDescription()
        {
            Literal_Description.Text = String.IsNullOrWhiteSpace(requestedAccount.Description) ? "N/A" : requestedAccount.Description;
        }

        protected void LoadFriends()
        {
            if (accountManager.GetFriends(requestedAccount).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var friend in accountManager.GetFriends(requestedAccount))
                {
                    sb.Append("<a href=\"/User.aspx?id=" + friend.ID + "\">" + friend.FirstName + " " + friend.LastName + " (" + friend.Username + ")" + "</a><br />");
                }
                Literal_Friends.Text = sb.ToString();
            }
            else
            {
                Literal_Friends.Text = "N/A<br />";
            }
        }

        protected void LoadProjects()
        {
            if (requestedAccount.ProjectsUsing.Select(c => c.Project).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var project in requestedAccount.ProjectsUsing.Select(c => c.Project))
                {
                    sb.Append("<a href=\"/Project.aspx?id=" + project.ID + "\">" + project.Name + "</a><br />");
                }
                Literal_Projects.Text = sb.ToString();
            }
            else
            {
                Literal_Projects.Text = "N/A<br />";
            }
        }

        //For now we'll do the current user's stuff.
        //Should eventually be what all your friends are up to.
        protected void LoadStream()
        {
            List<StreamItem> streamItems = new List<StreamItem>();

            //Foreach comment made.
            foreach (Comment comment in requestedAccount.CommentsMade)
            {
                StreamItem streamItem = new StreamItem();
                streamItem.dateTime = comment.Created;

                if (comment.ObjectType == (int)CommentObjectType.Project) //Comment is on a project.
                {
                    CSData.Project commentsProject = projectManager.GetProjectByID(comment.ObjectID);
                    streamItem.content = "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a></b> --> " + "<a href=\"/Project.aspx?id=" + commentsProject.ID + "\">" + commentsProject.Name + "</a>" + ": " + comment.Contents;
                }
                else //Comment is on a user.
                {
                    Account commentsAccount = accountManager.GetAccountByID(comment.ObjectID);
                    streamItem.content = "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a></b> --> " + "<a href=\"/User.aspx?id=" + commentsAccount.ID + "\">" + commentsAccount.FirstName + " " + commentsAccount.LastName + "</a>" + ": " + comment.Contents;
                }

                streamItems.Add(streamItem);
            }

            //Foreach following.
            foreach (ProjectUser projectUser in requestedAccount.ProjectsUsing)
            {
                if (projectUser.StartedFollowing != null)
                {
                    StreamItem streamItem = new StreamItem();
                    streamItem.dateTime = projectUser.StartedFollowing;
                    streamItem.content = "<a href=\"/User.aspx?id=" + projectUser.AccountID + "\">" + projectUser.Account.FirstName + " " + projectUser.Account.LastName + "</a> followed <a href=\"/Project.aspx?id=" + projectUser.ProjectID + "\">" + projectUser.Project.Name + "</a>.";
                    streamItems.Add(streamItem);
                }
            }

            //For each friending.
            foreach (FriendLink friendLink in requestedAccount.FriendLinks)
            {
                StreamItem streamItem = new StreamItem();
                streamItem.dateTime = friendLink.Created;
                streamItem.content = "<a href=\"/User.aspx?id=" + friendLink.AccountID + "\">" + friendLink.Account.FirstName + " " + friendLink.Account.LastName + "</a> friended <a href\"/User.aspx?id=" + friendLink.FriendID + "\">" + friendLink.Friend.FirstName + " " + friendLink.Friend.LastName + "</a>.";
                streamItems.Add(streamItem);
            }

            if (streamItems.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (StreamItem streamItem in streamItems.OrderByDescending(c => c.dateTime))
                {
                    sb.Append("<b>" + streamItem.dateTime.ToShortDateString() + " " + streamItem.dateTime.ToString("hh:mm tt") + "</b> - " + streamItem.content + "<br />");
                }
                Literal_Stream.Text = sb.ToString();
            }
            else
            {
                Literal_Stream.Text = "N/A<br />";
            }
        }
    }
}