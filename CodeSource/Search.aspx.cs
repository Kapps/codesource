﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;
using System.Text;

namespace CodeSource
{
    public partial class Search : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();
        ProjectManager projectManager = new ProjectManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Request["query"]))
            {
                PlaceHolder_SearchResults.Visible = false;
            }
            else
            {
                //Just so we don't overwrite their old query.
                if (!IsPostBack)
                {
                    TextBox_Search.Text = Request["query"];
                }

                Literal_CurrentPage.Text = Request["page"];
                Literal_TotalPages.Text = "Infinity";
                PlaceHolder_SearchResults.Visible = true;

                LoadSearch();
            }
        }

        protected void Button_Search_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx?query=" + TextBox_Search.Text + "&page=0");
        }

        protected void Button_PrevPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx?query=" + Request["query"] + "&page=" + (Int32.Parse(Request["page"]) - 1));
        }

        protected void Button_NextPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx?query=" + Request["query"] + "&page=" + (Int32.Parse(Request["page"]) + 1));
        }

        protected void LoadSearch()
        {
            StringBuilder sbAccounts = new StringBuilder();
            foreach (var account in accountManager.SearchAccounts(Request["query"]))
            {
                string description = String.IsNullOrWhiteSpace(account.Description) ? "N/A" : account.Description;
                sbAccounts.Append("<tr><td><a href=\"/User.aspx?id=" + account.ID + "\">" + account.FirstName + " " + account.LastName + "</a></td><td>" + account.Username + "</td><td>" + description + "</td></tr>");
            }
            Literal_SearchResultsAccounts.Text = sbAccounts.ToString();

            StringBuilder sbProjects = new StringBuilder();
            foreach (var project in projectManager.SearchProjects(Request["query"]))
            {
                sbProjects.Append("<tr><td><a href=\"/Project.aspx?id=" + project.ID + "\">" + project.Name + "</a></td><td><a href=\"/User.aspx?id=" + project.Account.ID+ "\">" + project.Account.FirstName + " " + project.Account.LastName + "</a></td><td>" + project.Description + "</td></tr>");
            }
            Literal_SearchResultsProjects.Text = sbProjects.ToString();
        }
    }
}