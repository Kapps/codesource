﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="CodeSource.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="row">
        <div class="span6">
            <div class="row">
                <div class="span"> <!--Basically, since this doesn't work as 1 or 2, we let it decide it's own size, and we know it'll be less than 2 based on image size.-->
                    <asp:Image ID="Image_Avatar" runat="server" Width="110" Height="110" />
                </div>
                <div class="span4">
                    <div class="row">
                        <div class="span4">
                            <h2><asp:Label ID="Label_FullName" runat="server" Text="Label"></asp:Label></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <asp:Label ID="Label_Username" runat="server" Text="Label"></asp:Label>
                            &nbsp
                            <!--Button text will change depending on friend status. No point in having two buttons for this.-->
                            <asp:Button ID="Button_Friend" runat="server" Text="Add/Remove Friend" CssClass="btn" OnClick="Button_Friend_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Description</h4>
                    <asp:Literal ID="Literal_Description" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Info</h4>
                    Email: 
                    <asp:HyperLink ID="HyperLink_Email" runat="server">HyperLink</asp:HyperLink>
                    <br />
                    Birthday: 
                    <asp:Label ID="Label_Birthday" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Stream</h4>
                    <asp:Literal ID="Literal_Stream" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Friends</h4>
                    <asp:Literal ID="Literal_Friends" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="span6">
                    <h4>Followed Projects</h4>
                    <asp:Literal ID="Literal_Projects" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class="span6">
                    <h4>Comments</h4>
                    <asp:Literal ID="Literal_Comments" runat="server" Text="Comment"></asp:Literal>
                    <br />
                    <asp:TextBox ID="TextBox_Comment" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <br />
                    <asp:Button ID="Button_AddComment" runat="server" Text="Submit" CssClass="btn" OnClick="Button_AddComment_Click"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
