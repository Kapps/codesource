﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CodeSource.Admin.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <p>Here we'll have the admin panel, with options to edit a project or user, so far.</p>
    <p>This separation ensures that admins can use the site as a normal individual would.</p>
</asp:Content>