﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource
{
    public partial class ViewFile : System.Web.UI.Page
    {
        ProjectManager projectManager = new ProjectManager();
        CSData.Project requestedProject = new CSData.Project();
        ModuleAccess moduleAccess = new ModuleAccess();
        SourceControlModule scm;
        int projId;
        string filePath;

        protected void Page_Load(object sender, EventArgs e)
        {
            projId = Int32.Parse(Request["projId"]);
            filePath = Request["filePath"];

            requestedProject = projectManager.GetProjectByID(projId);
            scm = moduleAccess.LoadModule<SourceControlModule>(requestedProject);
            //Pre_Code.InnerText =
            LoadFile();
        }

        protected void LoadFile()
        {
            CommitInfo commitInfo = scm.GetCommits().OrderByDescending(c => c.Date).First();

            foreach (var file in scm.GetAllFiles(commitInfo))
            {
                if (file.FilePath == filePath)
                    Pre_Code.InnerText = scm.GetFileContents(file);
            }
        }
    }
}