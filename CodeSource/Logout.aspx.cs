﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource
{
    public partial class Logout : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            accountManager.ActiveUser = null;
            Response.Redirect("LogReg.aspx");
        }
    }
}