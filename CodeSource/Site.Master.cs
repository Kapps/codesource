﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource
{
    public partial class Site : System.Web.UI.MasterPage
    {
        AccountManager accountManager = new AccountManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (accountManager.ActiveUser != null)
            {
                Label_Username.Text = accountManager.ActiveUser.FirstName + " " + accountManager.ActiveUser.LastName + " (" + accountManager.ActiveUser.Username + ")";

                PlaceHolder_LoggedIn.Visible = true;
                PlaceHolder_LoggedOut.Visible = false;
            }
            else
            {
                PlaceHolder_LoggedIn.Visible = false;
                PlaceHolder_LoggedOut.Visible = true;
            }
        }
    }
}