﻿using CSData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeSource
{
    public partial class Project : System.Web.UI.Page
    {
        AccountManager accountManager = new AccountManager();
        ProjectManager projectManager = new ProjectManager();
        CommentManager commentManager = new CommentManager();
        CSData.Project requestedProject = new CSData.Project();
        int projId;
        ModuleAccess moduleAccess = new ModuleAccess();
        Account activeUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Request["id"]))
                Response.Redirect("/Default.aspx");
            else
                projId = Int32.Parse(Request["id"]);

            this.activeUser = accountManager.ActiveUser;
            requestedProject = projectManager.GetProjectByID(projId);
            if (!IsPostBack)
            {
                var scm = moduleAccess.LoadModule<SourceControlModule>(requestedProject);
                RepositoryInfo? repositoryInfo = scm == null ? null : (RepositoryInfo?)scm.GetRepositoryInfo();
                IEnumerable<CommitInfo> commits = scm == null ? Enumerable.Empty<CommitInfo>() : scm.GetCommits().ToArray();
                var lastCommit = commits.OrderByDescending(c => c.Date).FirstOrDefault();
                IEnumerable<SourceControlFileSnapshot> files = commits.Any() ? scm.GetAllFiles(lastCommit).ToArray() : Enumerable.Empty<SourceControlFileSnapshot>();
                LoadDescription();
                LoadInfo(repositoryInfo);
                SetFollowButton();
                LoadFiles(lastCommit, files);
                LoadComments();
                LoadSimilar();
                LoadCommits(commits);
                SetCommentButton();
                SetCommentTextbox();
                LoadFollowers();
            }
        }

        private void LoadDescription()
        {
            Literal_Description.Text = requestedProject.Description;
        }

        private void LoadInfo(RepositoryInfo? repositoryInfo)
        {
            Label_Name.Text = requestedProject.Name;
            HyperLink_Owner.Text = requestedProject.Account.FirstName + " " + requestedProject.Account.LastName;
            HyperLink_Owner.NavigateUrl = "/User.aspx?id=" + requestedProject.Account.ID;
            Label_Language.Text = String.IsNullOrWhiteSpace(requestedProject.Languages) ? "N/A" : requestedProject.Languages;
            Label_Size.Text = (repositoryInfo == null) ? "N/A" : Math.Round((decimal)repositoryInfo.Value.SizeInBytes / 1024, 2) + " KB";

            if (repositoryInfo != null)
            {
                HyperLink_ExtLink.NavigateUrl = repositoryInfo.Value.WebLink.ToString();
                HyperLink_ExtLink.Text = repositoryInfo.Value.Provider.ToString();
            }
            else
            {
                HyperLink_ExtLink.Text = "N/A";
            } 
        }

        protected void Button_AddComment_Click(object sender, EventArgs e)
        {
            commentManager.CreateComment(requestedProject, activeUser, TextBox_Comment.Text);
            LoadComments();
        }

        protected void LoadComments()
        {
            var comments = commentManager.GetCommentsOnProject(requestedProject).OrderByDescending(c=>c.Created);
            if (comments.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (var comment in comments)
                {
                    sb.Append("<b>" + comment.Created.ToShortDateString() + " " + comment.Created.ToString("hh:mm tt") + "</b> - " + "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a>: " + comment.Contents + "<br />");
                }
                Literal_Comments.Text = sb.ToString();
            }
            else
            {
                Literal_Comments.Text = "N/A<br />";
            }
        }

        protected void Button_Follow_Click(object sender, EventArgs e)
        {
            if (!projectManager.IsUserFollowingProject(activeUser, requestedProject))
                projectManager.AddUser(requestedProject, activeUser, ProjectUsageType.Following);
            else
                projectManager.StopFollowing(activeUser, requestedProject);

            SetFollowButton();
        }

        protected void SetFollowButton()
        {
            if (activeUser != null)
            {
                if (!projectManager.IsUserFollowingProject(activeUser, requestedProject))
                    Button_Follow.Text = "Follow Project";
                else
                    Button_Follow.Text = "Unfollow Project";
            }
            else
            {
                Button_Follow.Visible = false;
            }
        }

        protected void SetCommentButton()
        {
            if (activeUser == null)
                Button_AddComment.Visible = false;
        }

        protected void SetCommentTextbox()
        {
            if (activeUser == null)
                TextBox_Comment.Visible = false;
        }

        protected void LoadFiles(CommitInfo commit, IEnumerable<SourceControlFileSnapshot> allFiles)
        {
            if (commit != null && allFiles.Any())
            {

                Label_CommitDate.Text = commit.Date.ToString();

                StringBuilder sb = new StringBuilder();
                foreach (var file in allFiles)
                {
                    sb.Append(new string('-', file.FilePath.Where(c => c == '/').Count()));
                    sb.Append("<a href=\"/ViewFile.aspx?projId=" + projId + "&filePath=" + file.FilePath + "\">" + file.FilePath + "</a><br />");
                    //Path.GetFileName() for above, but would need to do dirs too.
                }
                Literal_Files.Text = sb.ToString();
            }
            else
                Label_CommitDate.Text = "N/A";
        }

        protected void LoadCommits(IEnumerable<CommitInfo> commits)
        {
            if (commits.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (var commit in commits.OrderByDescending(c => c.Date).Take(5))
                {
                    sb.Append("<b>" + commit.Date.ToShortDateString() + " " + commit.Date.ToString("hh:mm tt") + " - " + commit.Author + "</b>: " + commit.Message + "<br />");
                }
                Literal_Commits.Text = sb.ToString();
            }
        }

        protected void LoadSimilar()
        {
            var similarProjects = projectManager.GetSimilarProjects(requestedProject).Take(10).ToArray();
            if (similarProjects.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (var project in similarProjects)
                {
                   sb.Append("<a href=\"Project.aspx?id=" + project.ID + "\">" + project.Name + "</a><br />");
                }
                Literal_SimilarProjects.Text = sb.ToString();
            }
            else
            {
                Literal_SimilarProjects.Text = "N/A<br />";
            }
        }

        protected void LoadFollowers()
        {
            if (requestedProject.ProjectUsers.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (var projectUser in requestedProject.ProjectUsers)
                {
                    sb.Append("<a href=\"/User.aspx?id=" + projectUser.AccountID + "\">" + projectUser.Account.FirstName + " " + projectUser.Account.LastName + " (" + projectUser.Account.Username + ")" + "</a><br />");
                }
                Literal_Followers.Text = sb.ToString();
            }
            else
            {
                Literal_Followers.Text = "N/A<br />";
            }
        }
    }
}