﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="CodeSource.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <div class="row">
        <div class="span12">
            <div class="row">
                <div class="span12">
                    <div id="searchText">
                        <p>Here you can type in keywords in order to find users or projects you may be interested in.</p>
                        <p>Simply enter them below and press search to view your results.</p>
                    </div>
                    <div>
                        <asp:TextBox ID="TextBox_Search" runat="server" CssClass="search-query"></asp:TextBox>
                        <asp:Button ID="Button_Search" runat="server" Text="Search" OnClick="Button_Search_Click" CssClass="btn btn-primary"/>
                    </div>
                </div>
            </div>
            <asp:PlaceHolder ID="PlaceHolder_SearchResults" runat="server">
            <div class="row">
                <div class="span12">
                    <!--
                    Viewing Page <asp:Literal ID="Literal_CurrentPage" runat="server"></asp:Literal> of <asp:Literal ID="Literal_TotalPages" runat="server"></asp:Literal>.
                    <asp:Button ID="Button_PrevPage" runat="server" Text="Previous" OnClick="Button_PrevPage_Click" CssClass="btn" />
                    <asp:Button ID="Button_NextPage" runat="server" Text="Next" OnClick="Button_NextPage_Click" CssClass="btn" />
                    <br />
                    <br />
                    -->
                    <div class="well">
                        <h4 class="search-header">Account Results</h4>
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Description</th>
                            </tr>
                            <!--Consider moving this up a couple levels, and just have it create table, but only if needed.-->
                            <asp:Literal ID="Literal_SearchResultsAccounts" runat="server"></asp:Literal>
                        </table>
                    </div>
                    <div class="well">
                        <h4 class="search-header">Project Results</h4>
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Owner</th>
                                <th>Description</th>
                            </tr>
                            <!--Consider moving this up a couple levels, and just have it create table, but only if needed.-->
                            <asp:Literal ID="Literal_SearchResultsProjects" runat="server"></asp:Literal>
                        </table>
                    </div>
                </div>
            </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
