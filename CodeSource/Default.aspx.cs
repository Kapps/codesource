﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;
using System.Text;

namespace CodeSource
{
    public partial class Default : System.Web.UI.Page
    {
        struct StreamItem {
            public DateTime dateTime { get; set; }
            public string content { get; set; }
        }

        AccountManager accountManager = new AccountManager();
        CommentManager commentManager = new CommentManager();
        ProjectManager projectManager = new ProjectManager();

        protected void Page_Load (object sender, EventArgs e) {
            if (accountManager.ActiveUser == null)
                Response.Redirect("/LogReg.aspx");

            if (!IsPostBack)
            {
                LoadAvatar();
                LoadInfo();
                LoadFriends();
                LoadComments();
                LoadProjects();
                LoadDescription();
                LoadStream();
            }
        }

        private void LoadAvatar()
        {
            Image_Avatar.ImageUrl = accountManager.ActiveUser.GetGravatarUrl(GravatarDefaultStyle.Wavatar) + "&s=200";
        }

        private void LoadInfo()
        {
            Label_FullName.Text = accountManager.ActiveUser.FirstName + " " + accountManager.ActiveUser.LastName;
            Label_Username.Text = "(" + accountManager.ActiveUser.Username + ")";
            Label_Birthday.Text = accountManager.ActiveUser.Birthdate == null ? "N/A" : ((DateTime)accountManager.ActiveUser.Birthdate).ToLongDateString();
            HyperLink_Email.Text = accountManager.ActiveUser.Email;
            HyperLink_Email.NavigateUrl = "mailto:" + accountManager.ActiveUser.Email;
        }

        protected void Button_AddComment_Click(object sender, EventArgs e)
        {
            commentManager.CreateComment(accountManager.ActiveUser, accountManager.ActiveUser, TextBox_Comment.Text);
            LoadComments();
        }

        protected void LoadComments()
        {
            if (commentManager.GetCommentsOnUser(accountManager.ActiveUser).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var comment in commentManager.GetCommentsOnUser(accountManager.ActiveUser).OrderByDescending(c => c.Created))
                {
                    sb.Append("<b>" + comment.Created.ToShortDateString() + " " + comment.Created.ToString("hh:mm tt") + "</b> - " + "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a>: " + comment.Contents + "<br />");
                }
                Literal_Comments.Text = sb.ToString();
            }
            else
            {
                Literal_Comments.Text = "N/A<br />";
            }
        }

        protected void LoadFriends()
        {
            if (accountManager.GetFriends(accountManager.ActiveUser).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var friend in accountManager.GetFriends(accountManager.ActiveUser))
                {
                    sb.Append("<a href=\"/User.aspx?id=" + friend.ID + "\">" + friend.FirstName + " " + friend.LastName + " (" + friend.Username + ")" + "</a><br />");
                }
                Literal_Friends.Text = sb.ToString();
            }
            else
            {
                Literal_Friends.Text = "N/A<br />";
            }
        }

        protected void LoadProjects()
        {
            if (accountManager.ActiveUser.ProjectsUsing.Select(c => c.Project).Count() != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var project in accountManager.ActiveUser.ProjectsUsing.Select(c => c.Project))
                {
                    sb.Append("<a href=\"/Project.aspx?id=" + project.ID + "\">" + project.Name + "</a><br />");
                }
                Literal_Projects.Text = sb.ToString();
            }
            else
            {
                Literal_Projects.Text = "N/A<br />";
            }
        }

        protected void LoadDescription()
        {
            Literal_Description.Text = String.IsNullOrWhiteSpace(accountManager.ActiveUser.Description) ? "N/A" : accountManager.ActiveUser.Description;
        }

        //For now we'll do the current user's stuff.
            //Should eventually be what all your friends are up to.
        protected void LoadStream()
        {
            List<StreamItem> streamItems = new List<StreamItem>();

            /* Excluding this for now. Too much repetition and possibly useless information.
            //Stream of project related activities.
            foreach (ProjectUser projectUser in accountManager.ActiveUser.ProjectsUsing)
            {
                //Foreach comment on project.
                foreach (Comment comment in commentManager.GetCommentsOnProject(projectUser.Project))
                {
                    StreamItem streamItem = new StreamItem();
                    streamItem.dateTime = comment.Created;
                    streamItem.content = "<b>" + comment.Owner.FirstName + " " + comment.Owner.LastName + " (" + comment.Owner.Username + ")</b> --> " + "<b>********</b>" + ": " + comment.Contents;
                    streamItems.Add(streamItem);
                }
                
                //Foreach follow on project.
                //foreach (
            }
            */

            //Stream of friend related activities.
            foreach (Account account in accountManager.GetFriends(accountManager.ActiveUser))
            {
                //Foreach comment made.
                foreach (Comment comment in account.CommentsMade)
                {
                    StreamItem streamItem = new StreamItem();
                    streamItem.dateTime = comment.Created;

                    if (comment.ObjectType == (int)CommentObjectType.Project) //Comment is on a project.
                    {
                        CSData.Project commentsProject = projectManager.GetProjectByID(comment.ObjectID);
                        streamItem.content = "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a></b> --> " + "<a href=\"/Project.aspx?id=" + commentsProject.ID + "\">" + commentsProject.Name + "</a>" + ": " + comment.Contents;
                    }
                    else //Comment is on a user.
                    {
                        Account commentsAccount = accountManager.GetAccountByID(comment.ObjectID);
                        streamItem.content = "<a href=\"/User.aspx?id=" + comment.Owner.ID + "\">" + comment.Owner.FirstName + " " + comment.Owner.LastName + "</a></b> --> " + "<a href=\"/User.aspx?id=" + commentsAccount.ID + "\">" + commentsAccount.FirstName + " " + commentsAccount.LastName + "</a>" + ": " + comment.Contents;
                    }

                    streamItems.Add(streamItem);
                }

                //Foreach following.
                foreach (ProjectUser projectUser in account.ProjectsUsing)
                {
                    if (projectUser.StartedFollowing != null)
                    {
                        StreamItem streamItem = new StreamItem();
                        streamItem.dateTime = projectUser.StartedFollowing;
                        streamItem.content = "<a href=\"/User.aspx?id=" + projectUser.AccountID + "\">" + projectUser.Account.FirstName + " " + projectUser.Account.LastName + "</a> followed <a href=\"/Project.aspx?id=" + projectUser.ProjectID + "\">" + projectUser.Project.Name + "</a>.";
                        streamItems.Add(streamItem);
                    }
                }

                //For each friending.
                foreach (FriendLink friendLink in account.FriendLinks)
                {
                    StreamItem streamItem = new StreamItem();
                    streamItem.dateTime = friendLink.Created;
                    streamItem.content = "<a href=\"/User.aspx?id=" + friendLink.AccountID + "\">" + friendLink.Account.FirstName + " " + friendLink.Account.LastName + "</a> friended <a href\"/User.aspx?id=" + friendLink.FriendID + "\">" + friendLink.Friend.FirstName + " " + friendLink.Friend.LastName + "</a>.";
                    streamItems.Add(streamItem);
                }
            }

            if (streamItems.Any())
            {
                StringBuilder sb = new StringBuilder();
                foreach (StreamItem streamItem in streamItems.OrderByDescending(c => c.dateTime))
                {
                    sb.Append("<b>" + streamItem.dateTime.ToShortDateString() + " " + streamItem.dateTime.ToString("hh:mm tt") + "</b> - " + streamItem.content + "<br />");
                }
                Literal_Stream.Text = sb.ToString();
            }
            else
            {
                Literal_Stream.Text = "N/A<br />";
            }
        }
    }
}