﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSData;

namespace CodeSource
{
    public partial class LogReg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AccountManager accountManager = new AccountManager();

            if (accountManager.ActiveUser != null)
                Response.Redirect("/Default.aspx");
        }

        protected void Button_LogIn_Click(object sender, EventArgs e)
        {
            try
            {
                AccountManager accountManager = new AccountManager();
                string errorDetails;
                accountManager.AttemptAuthenticate(TextBox_UsernameExisting.Text, TextBox_PasswordExisting.Text, out errorDetails);

                if (errorDetails == "Login Successful")
                {
                    Response.Redirect("/Default.aspx");
                }
                else
                {
                    ShowAlert("error", "Wrong username or password.");
                }
            }
            catch
            {
                ShowAlert("error", "There was a problem logging on.");
            }
        }

        protected void Button_Register_Click(object sender, EventArgs e)
        {
            try
            {
                AccountManager accountManager = new AccountManager();
                accountManager.CreateAccount(TextBox_Username.Text, TextBox_Password.Text, TextBox_Email.Text, TextBox_FirstName.Text, TextBox_LastName.Text);
            }
            catch (Exception exception)
            {
                ShowAlert2("error", "There was a problem creating this account.");
            }
        }

        protected void ShowAlert(string type, string message)
        {
            if (type == "success")
            {
                Div_Alert.Attributes["class"] = "alert alert-success alert-login";
            }
            else
            {
                Div_Alert.Attributes["class"] = "alert alert-error alert-login";
            }

            Div_Alert.InnerHtml = "<br />";
            Div_Alert.InnerText = message;

            Div_Alert.Visible = true;
        }

        protected void ShowAlert2(string type, string message)
        {
            if (type == "success")
            {
                Div_Alert2.Attributes["class"] = "alert alert-success alert-login";
            }
            else
            {
                Div_Alert2.Attributes["class"] = "alert alert-error alert-login";
            }

            Div_Alert2.InnerHtml = "<br />";
            Div_Alert2.InnerText = message;

            Div_Alert2.Visible = true;
        }
    }
}