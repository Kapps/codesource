﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewFile.aspx.cs" Inherits="CodeSource.ViewFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            prettyPrint();

            $(".prettyprint br").each(function (index, element) {
                /*Fix the newline stuff.*/
                $(element).replaceWith("<span class=\"str\">&lt;br /&gt</span>");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="server">
    <pre class="prettyprint" runat="server" id="Pre_Code">
    </pre>
</asp:Content>
