using System;

namespace CSData {

	/// <summary>
	/// Indicates what change occurred on a file in a commit.
	/// </summary>
	public enum FileSnapshotChangeType {
		/// <summary>
		/// The file was changed in an unknown way.
		/// </summary>
		Unknown,
		/// <summary>
		/// The file was modified.
		/// </summary>
		Modified = 1,
		/// <summary>
		/// The file was added.
		/// </summary>
		Added,
		/// <summary>
		/// The file was removed.
		/// </summary>
		Removed
	}
	
	/// <summary>
	/// Provides information about a single file registered in a source control repository at a specific revision.
	/// This struct is immutable.
	/// </summary>
	[Serializable]
	public struct SourceControlFileSnapshot {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.SourceControlFileSnapshot"/> struct.
		/// </summary>
		public SourceControlFileSnapshot(string Path, CommitInfo Commit, FileSnapshotChangeType ChangeType) {
			this.FilePath = Path;
			this.Commit = Commit;
			this.ChangeType = ChangeType;
		}
	
		/// <summary>
		/// Gets the path to this file, relative to the base repository, including file name and extension.
		/// </summary>
		public readonly string FilePath;
		
		/// <summary>
		/// Indicates the commit that this data applies for.
		/// </summary>
		public readonly CommitInfo Commit;
		
		/// <summary>
		/// Indicates how the file was changed.
		/// </summary>
		public readonly FileSnapshotChangeType ChangeType;
	}
}

