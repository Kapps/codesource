using System;

namespace CSData {
	/// <summary>
	/// Provides an implementation of the Authentication class that uses anonymous logins.
	/// </summary>
	[Serializable]
	public class AnonymousAuthentication : Authentication {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.AnonymousAuthentication"/> class.
		/// </summary>
		public AnonymousAuthentication() { }
	
		/// <summary>
		/// Performs any authentication required for the given Url, returning the Url with any authentication info.
		/// Note that the result should be considered to contain sensitive information.
		/// The original Url may or may not be altered (and as such, may contain sensitive information as well).
		/// </summary>
		public override Uri AuthenticateForUrl (Uri Url) {
			// Return just the Url as is, with any authentication details cleared.
			UriBuilder Builder = new UriBuilder(Url);
			Builder.UserName = null;
			Builder.Password = null;
			return Builder.Uri;
		}
	}
}

