using System;

namespace CSData {
	/// <summary>
	/// Provides an implementation of Authentication that uses a username and password to authenticate.
	/// It is highly recommended to avoid this form of authentication due to severe security issues.
	/// </summary>
	[Serializable]
	public class LoginAuthentication : Authentication {
	
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.LoginAuthentication"/> class.
		/// </summary>
		public LoginAuthentication() { }
		
		/// <summary>
		/// Gets or sets the username to authenticate with.
		/// </summary>
		public string Username { get; set; }
		
		/// <summary>
		/// Gets or sets the password to authenticate with.
		/// </summary>
		public string Password { get; set; }
	
		/// <summary>
		/// Performs any authentication required for the given Url, returning the Url with any authentication info.
		/// Note that the result should be considered to contain sensitive information.
		/// The original Url may or may not be altered (and as such, may contain sensitive information as well).
		/// </summary>
		public override Uri AuthenticateForUrl (Uri Url) {
			if(String.IsNullOrWhiteSpace(Username) && !String.IsNullOrWhiteSpace(Password))
				throw new ArgumentException("Unable to set a password with no username for login authentication.");
			string AuthString = Username;
			if(!String.IsNullOrWhiteSpace(Password))
				AuthString += ":" + Password;
			return Url;
		}
	}
}

