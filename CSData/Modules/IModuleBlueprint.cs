using System;

namespace CSData {
	/// <summary>
	/// Indicates that the implementing type acts as a ModuleBlueprint for a ProjectModule.
	/// All module blueprints are implicitly initialized upon program startup.
	/// </summary>
	public interface IModuleBlueprint {
	}
}

