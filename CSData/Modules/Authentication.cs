using System;


namespace CSData {
	/// <summary>
	/// An abstract class indicating how authentication should be performed for a module.
	/// </summary>
	[Serializable]
	public abstract class Authentication {
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.Authentication"/> class.
		/// </summary>
		public Authentication() { }
	
		/// <summary>
		/// Performs any authentication required for the given Url, returning the Url with any authentication info.
		/// Note that the result should be considered to contain sensitive information.
		/// The original Url may or may not be altered (and as such, may contain sensitive information as well).
		/// </summary>
		public abstract Uri AuthenticateForUrl(Uri Url);
	}
}

