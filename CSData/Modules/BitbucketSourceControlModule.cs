using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSData {
	/// <summary>
	/// Provides an implementation of SourceControlModule that gets it's data from a Bitbucket repository.
	/// </summary>
	[Serializable]
	public class BitbucketSourceControlModule : SourceControlModule {
		
		// Used so we can send SSL requests, since Mono dislikes the idea of trusting... anything.
		// Instead, we say no need for security and trust everything!
		private static bool Validator (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			return true;
		}
		
		static BitbucketSourceControlModule() {
			ServicePointManager.ServerCertificateValidationCallback = Validator;
		}
	
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.BitbucketSourceControlModule"/> class.
		/// </summary>
		public BitbucketSourceControlModule () { }
		
		/// <summary>
		/// Gets or sets the owner of the repository on Bitbucket.
		/// This is used to determine the Url for the repository.
		/// </summary>
		public string BitbucketOwner { get; set; }
		
		/// <summary>
		/// Gets or sets the name of the repository on Bitbucket.
		/// This is used to determine the Url for the repository.
		/// </summary>
		public string BitbucketName { get; set; }
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.BitbucketSourceControlModule"/> class
		/// with the specified Owner, Repository, and Authentication scheme, populating the remainder of the values automatically.
		/// </summary>
		public BitbucketSourceControlModule(string BitbucketOwner, string BitbucketRepository, Authentication Authentication) {
			this.Slug = BitbucketRepository.ToLower();
			this.BitbucketOwner = BitbucketOwner;
			this.BitbucketName = BitbucketRepository;
			this.Authentication = Authentication;
		}
		
		/// <summary>
		/// Gets basic information about the repository.
		/// </summary>
		public override RepositoryInfo GetRepositoryInfo () {
			JObject ResponseData = GetResponseData();
			RepositoryInfo Result = new RepositoryInfo(
				(string)ResponseData["language"],
				(SourceControlProvider)Enum.Parse(typeof(SourceControlProvider), (string)ResponseData["scm"], true),
				(int)ResponseData["size"],
				DateTime.Parse((string)ResponseData["utc_last_updated"]).ToLocalTime(),
				(string)ResponseData["description"],
				new Uri("https://bitbucket.org/" + (string)ResponseData["owner"] + "/" + (string)ResponseData["slug"])
			);
			return Result;
		}
		
		/// <summary>
		/// Gets all files tracked by this repository, at the commit they were last changed.
		/// </summary>
		public override IEnumerable<SourceControlFileSnapshot> GetAllFiles (CommitInfo Commit) {
			return LoadFiles(Commit, "/");
		}
		
		private IEnumerable<SourceControlFileSnapshot> LoadFiles(CommitInfo Commit, string CurrPath) {
			JObject Response = GetResponseData("src", Commit.Revision, CurrPath);
			// TODO: At the moment this is wrong.
			// But loading the commit for every single file would be a pretty much guaranteed ban from Bitbucket's API.
			// We don't have a way of getting both the commit and the file; just the file and revision.
			// So, we'll just go ahead and use Commit instead for the moment.
			foreach(JObject FileInfo in Response["files"]) {
				string FilePath = (string)FileInfo["path"];
				SourceControlFileSnapshot Snapshot = new SourceControlFileSnapshot(FilePath, Commit, CSData.FileSnapshotChangeType.Unknown);
				yield return Snapshot;
			}
			foreach(string SubDirectory in Response["directories"]) {
				string DirectoryPath = CurrPath + "/" + SubDirectory;
				foreach(var Snapshot in LoadFiles(Commit, DirectoryPath))
					yield return Snapshot;
			}
		}
		
		/// <summary>
		/// Gets the commits that occurred on this source control repository.
		/// </summary>
		public override IEnumerable<CommitInfo> GetCommits () {
			JObject Result = GetResponseData("changesets", "?limit=20");
			foreach(JObject ChangeSet in Result["changesets"]) {
				string Message = (string)ChangeSet["message"];
				string Node = (string)ChangeSet["node"];
				DateTime Time = DateTime.Parse((string)ChangeSet["utctimestamp"]).ToLocalTime();
				JToken AuthorToken;
				string Author;
				if(ChangeSet.TryGetValue("author", out AuthorToken))
					Author = (string)AuthorToken;
				else
					Author = (string)ChangeSet["raw_author"];
				
				List<SourceControlFileSnapshot> FileSnapshots = new List<SourceControlFileSnapshot>();
				CommitInfo Commit = new CommitInfo(Author, Time, Node, Message, FileSnapshots);
				foreach(JObject FileChange in ChangeSet["files"]) {
					string TypeString = (string)FileChange["type"];
					FileSnapshotChangeType ChangeType;
					switch(TypeString.ToLower().Trim ()) {
						case "added":
							ChangeType = FileSnapshotChangeType.Added;
							break;
						case "removed":
						case "deleted":
							ChangeType = FileSnapshotChangeType.Removed;
							break;
						case "modified":
							ChangeType = FileSnapshotChangeType.Modified;
							break;
						default:
							ChangeType = FileSnapshotChangeType.Unknown;
							break;
					}
					string RelativePath = (string)FileChange["file"];
					SourceControlFileSnapshot Snapshot = new SourceControlFileSnapshot(RelativePath, Commit, ChangeType);
					FileSnapshots.Add(Snapshot);
				}
				yield return Commit;
			}
		}
		
		/// <summary>
		/// Returns the contents of the specified file.
		/// </summary>
		public override string GetFileContents (SourceControlFileSnapshot File) {
			string Response = ReadRawResponse("raw", File.Commit.Revision, File.FilePath);
			return Response;
		}

		private JObject GetResponseData(params string[] Segments) {
			string Contents = ReadRawResponse(Segments);
			//JavaScriptSerializer Serializer = new JavaScriptSerializer();
			//return Serializer.Deserialize<dynamic>(Contents);
			return JObject.Parse(Contents);
		}
		
		private string ReadRawResponse(params string[] Segments) {
			Uri Url = GetUnauthenticatedUrl(Segments);
			Url = Authentication.AuthenticateForUrl(Url);
			var Request = (HttpWebRequest)HttpWebRequest.Create(Url);
			var Response = (HttpWebResponse)Request.GetResponse();
			int StatusCode = (int)Response.StatusCode;
			if(StatusCode / 100 != 2) {
				// The status codes defined by the Bitbucket 1.0 API are all listed here.
				// Reference: https://confluence.atlassian.com/display/BITBUCKET/Using+the+Bitbucket+REST+APIs#UsingtheBitbucketRESTAPIs-ResponseCodes
				switch(StatusCode) {
					case 400:
						throw new InvalidDataException("An invalid request was made to the Bitbucket API. This is an internal error.");
					case 401:
						throw new UnauthorizedAccessException("The request requires authentication, and either no credentials were provided or they were invalid.");
					case 403:
						throw new UnauthorizedAccessException("Authentication for the request succeeded, but you do not have sufficient access to perform this operation.");
					case 404:
						throw new FileNotFoundException("The requested Uri was not found. Please make sure the Owner and Repository Name fields are correct.");
					default:
						throw new InvalidOperationException("The BitBucket API returned a result not expected. The result was " + StatusCode + " - " + Response.StatusDescription + ".");
				}
			}
			
			using(var Reader = new StreamReader(Response.GetResponseStream()))
				return Reader.ReadToEnd();
		}
		
		private Uri GetUnauthenticatedUrl(params string[] Segments) {
			string BaseUrl = "https://api.bitbucket.org/1.0/repositories/" + BitbucketOwner + "/" + Slug;
			UriBuilder Builder = new UriBuilder(BaseUrl);
			if(Segments == null)
				Segments = new string[0];
			bool IsQuery = false;
			foreach(var Segment in Segments) {
				if(IsQuery || Segment[0] == '?' || Segment[0] == '&') {
					IsQuery = true;
					Builder.Query += Segment.Substring(1);
				} else
					Builder.Path += "/" + Segment;
			}
			return Builder.Uri;
		}

		private string Slug;
	}
}

