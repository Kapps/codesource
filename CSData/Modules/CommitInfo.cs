using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace CSData {
	/// <summary>
	/// Provides information about a single commit.
	/// </summary>
	[Serializable]
	public sealed class CommitInfo {
	
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.CommitInfo"/> struct.
		/// </summary>
		public CommitInfo(string Author, DateTime Date, string Revision, string Message, IEnumerable<SourceControlFileSnapshot> ChangedFiles) {
			this.Author = Author;
			this.Date = Date;
			this.Revision = Revision;
			this.Message = Message;
			this.ChangedFiles = ChangedFiles;
		}
	
		/// <summary>
		/// Gets the author that performed this commit.
		/// </summary>
		public string Author { get; private set; }
		
		/// <summary>
		/// Gets the date that this commit occurred.
		/// </summary>
		public DateTime Date { get; private set; }
		
		/// <summary>
		/// Gets the revision that this commit resulted in.
		/// </summary>
		public string Revision { get; private set; }
		
		/// <summary>
		/// Gets the message that was specified for this commit.
		/// </summary>
		public string Message { get; private set; }
		
		/// <summary>
		/// Gets the files that have changed in this commit.
		/// </summary>
		public IEnumerable<SourceControlFileSnapshot> ChangedFiles { get; private set; }
	}
}

