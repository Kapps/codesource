using System;
using System.Collections.Generic;

namespace CSData {

	/// <summary>
	/// Provides a base module for source control info.
	/// </summary>
	[Serializable]
	public abstract class SourceControlModule : IModuleBlueprint {	

		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.SourceControlModule"/> class.
		/// </summary>
		public SourceControlModule() { }
	
		/// <summary>
		/// Indicates how to authenticate to the given Url.
		/// </summary>
		public Authentication Authentication { get; set; }
		
		/// <summary>
		/// Gets basic information about the repository.
		/// </summary>
		public abstract RepositoryInfo GetRepositoryInfo();
		
		/// <summary>
		/// Gets the commits that occurred on this source control repository.
		/// </summary>
		public abstract IEnumerable<CommitInfo> GetCommits();
		
		/// <summary>
		/// Gets all files tracked by this repository, at the commit they were last changed.
		/// </summary>
		public abstract IEnumerable<SourceControlFileSnapshot> GetAllFiles(CommitInfo Commit);
		
		/// <summary>
		/// Returns the contents of the specified file.
		/// </summary>
		public abstract string GetFileContents(SourceControlFileSnapshot File);
	}
}

