using System;

namespace CSData {
	
	/// <summary>
	/// Indicates the source control provider for a repository.
	/// </summary>
	public enum SourceControlProvider {
		/// <summary>
		/// The repository is using an unknown source control provider.
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// Git is being used for this repository.
		/// </summary>
		Git = 1,
		/// <summary>
		/// Mercurial, or hg, is being used for this repository.
		/// </summary>
		Mercurial = 2,
		/// <summary>
		/// Svn is being used for this repository.
		/// </summary>
		Svn = 4,
        /// <summary>
        /// An alias for Mercurial.
        /// </summary>
        HG = Mercurial
	}
	
	/// <summary>
	/// Provides information about a source control repository.
	/// This struct is immutable.
	/// </summary>
	[Serializable]
	public struct RepositoryInfo {
	
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.RepositoryInfo"/> struct.
		/// </summary>
		public RepositoryInfo(string Language, SourceControlProvider Provider, long SizeInBytes, DateTime LastUpdated, string Description, Uri WebLink) {
			this.Language = Language;
			this.Provider = Provider;
			this.SizeInBytes = SizeInBytes;
			this.LastUpdated = LastUpdated;
			this.Description = Description;
			this.WebLink = WebLink;
		}
	
		/// <summary>
		/// Indicates the programming language being used for this repository, such as D or C#.
		/// </summary>
		public readonly string Language;
		
		/// <summary>
		/// Indicates the type of this repository.
		/// </summary>
		public readonly SourceControlProvider Provider;
		
		/// <summary>
		/// Indicates the size of this repository, in bytes.
		/// </summary>
		public readonly long SizeInBytes;
		
		/// <summary>
		/// Indicates when this repository was last updated.
		/// </summary>
		public readonly DateTime LastUpdated;
		
		/// <summary>
		/// Provides a description of this repository.
		/// </summary>
		public readonly string Description;
		
		/// <summary>
		/// Provides a link that can be used for online viewing of this repository from a browser.
		/// </summary>
		public readonly Uri WebLink;
	}
}

