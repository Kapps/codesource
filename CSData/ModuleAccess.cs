using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace CSData {
	/// <summary>
	/// Provides access to Project Module information.
	/// </summary>
	public class ModuleAccess : AccessBase {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.ModuleAccess"/> class.
		/// </summary>
		public ModuleAccess() : base() {
			lock(typeof(ModuleAccess)) {
				if(!IsInitialized)
					Initialize();
			}
		}
		
		/// <summary>
		/// Gets all blueprints available for project modules.
		/// </summary>
		public IQueryable<ModuleBlueprint> GetAllBlueprints() {
			return Context.ModuleBlueprints;
		}
		
		/// <summary>
		/// Gets a blueprint for the given object, or null if none available.
		/// </summary>
		public ModuleBlueprint GetBlueprint(object Module) {
			Type t = Module.GetType();
			return GetBlueprint(t.AssemblyQualifiedName);
		}
		
		/// <summary>
		/// Returns the blueprint with the specified AssemblyQualifiedName, or null if not found.
		/// </summary>
		public ModuleBlueprint GetBlueprint(string AssemblyQualifiedName) {
			return Context.ModuleBlueprints.FirstOrDefault(c=>c.AssemblyQualifiedName == AssemblyQualifiedName);
		}
		
		/// <summary>
		/// Adds a module of the given type to the specified project.
		/// This is simply a shortcut to retrieving the blueprint, adding the module with the blueprint, and returning the result casted.
		/// </summary>
		public T CreateModule<T>(Project Proj) {
			string AssemblyQualifiedName = typeof(T).AssemblyQualifiedName;
			ModuleBlueprint Blueprint = Context.ModuleBlueprints.FirstOrDefault(c=>c.AssemblyQualifiedName == AssemblyQualifiedName);
			if(Blueprint == null)
				throw new ArgumentException("No blueprint available for " + typeof(T).Name + ".");
			IModuleBlueprint Result = CreateModule(Proj, Blueprint);
			return (T)Result;
		}
		
		/// <summary>
		/// Loads the first module with type T and the given name from the specified project, or null if not found.
		/// </summary>
		public T LoadModule<T>(Project Proj) where T : class, IModuleBlueprint {
			foreach(var Module in Proj.ProjectModules) {
				var Blueprint = Module.ModuleBlueprint;
				Type ModuleType = GetTypeForBlueprint(Blueprint);
				if(ModuleType == typeof(T) || ModuleType.IsSubclassOf(typeof(T))) {
					object Result = LoadModuleFromSerializedData(Module.SerializedData.ToArray());
					return (T)Result;
				}					
			}
			return null;
		}
		
		/// <summary>
		/// Adds a module with the given blueprint to the specified project, returning the resulting object.
		/// </summary>
		public IModuleBlueprint CreateModule(Project Proj, ModuleBlueprint Blueprint) {
			Type ModuleType = GetTypeForBlueprint(Blueprint);
			IModuleBlueprint Instance = (IModuleBlueprint)Activator.CreateInstance(ModuleType);
			return Instance;
		}
		
		/// <summary>
		/// Adds the specified module to the given project's list of modules.
		/// </summary>
		public void AddModule<T> (Project Proj, T Module) where T : IModuleBlueprint {
			byte[] SerializedData = GetSerializedModuleData(Module);
			ProjectModule ProjMod = new ProjectModule () {
				BlueprintID = GetBlueprint(Module).ID,
				Created = DateTime.Now,
				ProjectID = Proj.ID,
				SerializedData = SerializedData
			};
			Context.ProjectModules.InsertOnSubmit (ProjMod);
			SubmitChanges ();
		}
		
		private Type GetTypeForBlueprint(ModuleBlueprint Blueprint) {
			Type Result = Type.GetType(Blueprint.AssemblyQualifiedName);
			if(Result == null)
				throw new ArgumentException("Unable to find the resulting type for \'" + Blueprint.AssemblyQualifiedName + "\''.");
			return Result;
		}
		
		private byte[] GetSerializedModuleData (object Module) {
			BinaryFormatter bf = new BinaryFormatter ();
			using (MemoryStream ms = new MemoryStream()) {
				bf.Serialize (ms, Module);
				ms.Position = 0;
				byte[] Contents = new byte[ms.Length];
				ms.Read (Contents, 0, (int)ms.Length);
				return Contents;
			}
		}
		
		private IModuleBlueprint LoadModuleFromSerializedData(byte[] Data) {
			BinaryFormatter bf = new BinaryFormatter();
			using(var ms = new MemoryStream(Data)) {
				var Result = (IModuleBlueprint)bf.Deserialize(ms);
				return Result;
			}
		}
		
		private static void Initialize() {
			Dictionary<string, Type> BlueprintTypes = new Dictionary<string, Type>();
			using(var Context = ConnectionManager.CreateContext()) {
				// First, find everything that has the IModuleBlueprint interface.
				foreach(Type t in Assembly.GetCallingAssembly().GetTypes()) {
					if(t.GetInterface(typeof(IModuleBlueprint).Name) != null)
						BlueprintTypes.Add(t.AssemblyQualifiedName, t);
				}
				// Then, make sure that all ModuleBlueprints in the database still have a matching type.
				foreach(var ModuleBlueprint in Context.ModuleBlueprints) {
					if(!BlueprintTypes.Remove(ModuleBlueprint.AssemblyQualifiedName)) {
						if(ModuleBlueprint.ProjectModules.Any()) {
							throw new MissingMemberException("The ModuleBlueprint \'" + ModuleBlueprint.Name + "\' did not have it's representing type found, "
							                                 + "but projects are using it. This must be manually fixed.");
						} else {
							RemoveBlueprint(Context, ModuleBlueprint);
						}
					}
				}
				// Lastly, for any new types deriving from ModuleBlueprint, manually add them.
				foreach(var KVP in BlueprintTypes) {
					CreateBlueprint(Context, KVP.Value.Name, KVP.Value.AssemblyQualifiedName);
				}
			}
			IsInitialized = true;
		}
	
		private static void RemoveBlueprint(CodeSourceDataContext Context, ModuleBlueprint Blueprint) {
			if(Context.ProjectModules.Any (c=>c.BlueprintID == Blueprint.ID))
				throw new InvalidOperationException("Unable to remove a blueprint when projects are still using it.");
			Context.ModuleBlueprints.DeleteOnSubmit(Blueprint);
			Context.SubmitChanges();
		}
		
		private static ModuleBlueprint CreateBlueprint(CodeSourceDataContext Context, string Name, string AssemblyQualfifiedName) {
			ModuleBlueprint Blueprint = new ModuleBlueprint() {
				AssemblyQualifiedName = AssemblyQualfifiedName,
				Name = Name
			};
			Type t = Type.GetType(Blueprint.AssemblyQualifiedName);
			if(t == null)
				throw new ArgumentOutOfRangeException("Unknown type to create a Blueprint for.");
			Context.ModuleBlueprints.InsertOnSubmit(Blueprint);
			Context.SubmitChanges();
			return Blueprint;
		}
		
		private static bool IsInitialized = false;
	}
}

