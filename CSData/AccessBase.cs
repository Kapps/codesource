using System;

namespace CSData {
	/// <summary>
	/// Provides the base class for any data-access classes, giving access to a single DataContext.
	/// All values returned from Access classes are assumed to require any changes to be saved.
	/// Methods that create, update, or delete values will also submit any changed values as they share a DataContext.
	/// Access classes are not thread-safe.
	/// </summary>
	public class AccessBase : IDisposable {
	
		/// <summary>
		/// Initializes a new instance of the <see cref="CSData.AccessBase"/> class.
		/// </summary>
		public AccessBase() {
			this._Context = ConnectionManager.CreateContext();
		}
		
		/// <summary>
		/// Gets the DataContext that is being used for this Access class.
		/// </summary>
		public CodeSourceDataContext Context {
			get { return _Context; }
		}
		
		/// <summary>
		/// Submits any updates made to projects created by this Access class back to the database.
		/// Though this method is automatically called when creating or deleting instances, it is useful when manually changing
		/// instances returned by this access class.
		/// </summary>
		public void SubmitChanges() {
			Context.SubmitChanges();
		}
		
		/// <summary>
		/// Releases all resource used by the <see cref="CSData.AccessBase"/> object.
		/// Any changes that have not been persisted through SubmitChanges will be discarded.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="CSData.AccessBase"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="CSData.AccessBase"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="CSData.AccessBase"/> so the garbage
		/// collector can reclaim the memory that the <see cref="CSData.AccessBase"/> was occupying.
		/// </remarks>
		public void Dispose() {
			_Context.Dispose ();
		}
		
		private CodeSourceDataContext _Context;
	}
}

