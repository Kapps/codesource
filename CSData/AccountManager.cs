using System;
using System.Web;
using System.Data.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;	

namespace CSData {

	/// <summary>
	/// Indicates what network is being used for external authentication.
	/// </summary>
	public enum AuthNetwork {
		/// <summary>
		/// The network is an unknown network.
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// The network is Facebook.
		/// </summary>
		Facebook = 1,
		/// <summary>
		/// The network is Google Plus.
		/// </summary>
		Google = 2
	}
	
	/// <summary>
	/// Provides access to account information.
	/// </summary>
	public class AccountManager : AccessBase {
		
		/// <summary>
		/// Gets or sets the currently active user for the active HttpContext, or null if the user is either not logged in or no HttpContext is available.
		/// An exception will be thrown if attempting to set with no HttpContext availble.
		/// </summary>
		public Account ActiveUser {
			get { 
				HttpContext Context = HttpContext.Current;
				if(Context == null)
					return null;
				var Session = Context.Session;
				if(Session == null)
					return null;
				object AccountID = Session["AccountID"];
				if(AccountID == null || AccountID.GetType() != typeof(int))
					return null;
				return GetAccountByID((int)AccountID);
			} set {
				HttpContext Context = HttpContext.Current;
				if(Context == null)
					throw new InvalidOperationException("Unable to set the active account with no valid HttpContext.");
				var Session = Context.Session;
				// If null, log them out; otherwise, log them in.
				if(value == null)
					Session.Remove("AccountID");
				else 
					Session["AccountID"] = value.ID;
			}
		}
		
		/// <summary>
		/// Gets the account with the given ID.
		/// </summary>
		public Account GetAccountByID(int ID) {
			return Context.Accounts.FirstOrDefault(c=>c.ID == ID);
		}
		
		/// <summary>
		/// Links the given account to the specified network.
		/// </summary>
		/// <param name='Network'>The social network to link the account to.</param>
		/// <param name='Account'>The account to link to.</param>
		/// <param name='ExternalID'>A unique, persistent, ID assigned to the user on Network.</param>
		/// <param name='Identifier'>An identifier that may be used to refer to this user on the given Social Network.</param>
		public AuthLink LinkAccountToNetwork(AuthNetwork Network, Account Account, string ExternalID, string Identifier) {
			AuthLink Link = new AuthLink();
			Link.AccountID = Account.ID;
			Link.ExternalIdentifier = Identifier;
			Link.ExternalID = ExternalID;
			Link.AuthType = (int)Network;
			Context.AuthLinks.InsertOnSubmit(Link);
			Context.SubmitChanges();
			return Link;
		}

		/// <summary>
		/// Returns all of the users who have an AuthLink to Network, with the external ID being contained by Identifiers.
		/// </summary>
		public IEnumerable<Account> GetUsersWithLinkedIdentifier (AuthNetwork Network, IEnumerable<string> Identifiers) {
			return Context.AuthLinks.Where (c=>c.AuthType == (int)Network && Identifiers.Contains(c.ExternalID))
				.Select(c=>c.Account);
		}

		/// <summary>
		/// Returns the specified account's link to the given network, or null if not linked.
		/// </summary>
		public AuthLink GetNetworkLink (Account Account, AuthNetwork Network) {
			return Context.AuthLinks.FirstOrDefault(c=>c.AccountID == Account.ID && c.AuthType == (int)Network);
		}

		/// <summary>
		/// Generates a secure salt to be used for authentication.
		/// </summary>
		public string GenerateSalt() {
			RNGCryptoServiceProvider Provider = new RNGCryptoServiceProvider();
			byte[] Data = new byte[16];
			Provider.GetBytes(Data);
			StringBuilder Builder = new StringBuilder(Data.Length * 2);
			foreach(var Byte in Data)
				Builder.AppendFormat("{0:X2}", Byte);
			return Builder.ToString();
		}
		
		/// <summary>
		/// Creates an account with the given details, throwing an exception if any errors occur (such as a name being in use).
		/// If creating the account succeeds, the user will be logged in.
		/// </summary>
		public Account CreateAccount(string Username, string PassHash, string Email, string FirstName, string LastName) {
			Username = Username.Trim();
			int NumSalts = new Random().Next (100, 200);
			string PassSalt = GenerateSalt();
			if(Username == null || Username.Length < 3 || Username.Length > 16)
				throw new ArgumentException("Username must be between 3 and 16 characters.");
			if(PassHash.Length < 16 || PassSalt.Length < 16 || PassHash.Length > 64 || PassSalt.Length > 64 || NumSalts > 10000 || NumSalts < 100)
				throw new ArgumentException("Invalid password data.");
			PassHash = GetSaltedPassword(PassHash, NumSalts, PassSalt);
			if(String.IsNullOrWhiteSpace(Email))
				Email = null;
			Account Acc = new Account();
			Acc.Email = Email;
			Acc.PassHash = PassHash;
			Acc.PassSalt = PassSalt;
			Acc.NumSalts = NumSalts;
			Acc.Username = Username;
			Acc.FirstName = FirstName;
			Acc.LastName = LastName;
			Context.Accounts.InsertOnSubmit(Acc);
			Context.SubmitChanges();
			ActiveUser = Acc;
			return Acc;
		}
		
		/// <summary>
		/// Attempts to authenticate the current session with the given username and password hash.
		/// If successful, returns the resulting account. Otherwise, ErrorDetails is set to a human readable description of the error.
		/// </summary>
		public Account AttemptAuthenticate (string Username, string PassHash, out string ErrorDetails) {
			Username = Username.Trim ();
			PassHash = PassHash.Trim ();
			var Account = Context.Accounts.FirstOrDefault(c=>c.Username == Username);
			if(Account == null) {
				ErrorDetails = "No account with that name was found.";
				return null;
			}
			PassHash = GetSaltedPassword(PassHash, Account.NumSalts, Account.PassSalt);
			if(!String.Equals(Account.PassHash, PassHash, StringComparison.InvariantCultureIgnoreCase)) {
				ErrorDetails = "An account exists with that username, but the password was incorrect. Please note that the password is case sensitive.";
				return null;
			}
			ActiveUser = Account;
			ErrorDetails = "Login Successful";
			return Account;
		}
		
		/// <summary>
		/// Adds friend to the specified Account's friends list.
		/// </summary>
		public void AddFriend(Account User, Account Friend) {
			if(User == Friend)
				throw new InvalidOperationException("Unable to add yourself as a friend.");
			FriendLink Link = new FriendLink() {
				AccountID = User.ID,
				FriendID = Friend.ID
			};
			Context.FriendLinks.InsertOnSubmit(Link);
			Context.SubmitChanges();
		}

		/// <summary>
		/// Removes the given friend from the user's friends list.
		/// </summary>
		public void RemoveFriend(Account User, Account Friend) {
			var FriendLink = Context.FriendLinks.SingleOrDefault(c=>c.AccountID == User.ID && c.FriendID == Friend.ID);
			if(FriendLink == null)
				throw new InvalidOperationException("This user is not your friend.");
			Context.FriendLinks.DeleteOnSubmit(FriendLink);
			Context.SubmitChanges();
		}
		
		/// <summary>
		/// Returns the friends that the given user has.
		/// </summary>
		public IEnumerable<Account> GetFriends(Account User) {
			return User.FriendLinks.Select(c=> c.Friend);
		}

		/// <summary>
		/// Determines whether User is friends with PotentialFriend.
		/// </summary>
		public bool IsUserFriendsWith (Account User, Account PotentialFriend) {
			return Context.FriendLinks.Any(c=>c.AccountID == User.ID && c.FriendID == PotentialFriend.ID);
		}

        /// <summary>
        /// Searches for any accounts that match the given query, separated by white-space or commas.
        /// The result is implementation defined, but the most accurate results will be returned first.
        /// </summary>
        public IEnumerable<Account> SearchAccounts(string Query) {
            // TODO: This is an almost complete copy and paste of ProjectManager.SearchProjects.
            // The reason for this is because refactoring it would be too much effort for a function
            // that will get completely replaced eventually, to operate on the server instead, likely using full text search.
            string[] Terms = Query.Split(new char[] { ',', ' ', '\t', '\n', '\r', '-' }, StringSplitOptions.RemoveEmptyEntries);
            Terms = Terms.Select(c => RemoveNonLetters(c)).Where(c => !String.IsNullOrWhiteSpace(c)).ToArray();
            var Results = Context.Accounts.AsEnumerable().Select(c =>
                new
                {
                    Count = Terms.Where(d => c.Username.ToLower().Contains(d)).Count() * 20
                          + Terms.Where(d => c.FirstName.ToLower().Contains(d) || c.LastName.ToLower().Contains(d)).Count() * 5,
                    Account = c
                })
                .Where(c => c.Count > 0)
                .OrderByDescending(c => c.Count)
                .Select(c => c.Account);
            return Results;
        }

        private string RemoveNonLetters(string Input) {
            return new string(Input.Where(c => char.IsLetterOrDigit(c)).ToArray()).ToLower();
        }
		
		private string GetSaltedPassword(string OriginalHash, int NumSalts, string Salt) {
			var Hasher = SHA256.Create();
			byte[] PasswordData = Encoding.UTF8.GetBytes(OriginalHash);
			for(int i = 0; i < NumSalts; i++) {
				byte[] NewData = Hasher.ComputeHash(PasswordData);
				PasswordData = NewData;
			}
			StringBuilder Builder = new StringBuilder(PasswordData.Length * 2);
			foreach(var Byte in PasswordData)
				Builder.AppendFormat("{0:X2}", Byte);
			return Builder.ToString();
		}
	}
}

