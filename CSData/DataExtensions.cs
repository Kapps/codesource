using System;
using System.Security.Cryptography;
using System.Text;

namespace CSData {

	/// <summary>
	/// Indicates the type of gravatar to generate if no match is found for that user's email.
	/// </summary>
	public enum GravatarDefaultStyle {
		/// <summary>
		/// A simple abstract pattern.
		/// </summary>
		Identicon,
		/// <summary>
		/// The face of a cartoony monster.
		/// </summary>
		Wavatar,
		/// <summary>
		/// The full body of a cartoony 2D monster.
		/// </summary>
		Monsterid
	}

	/// <summary>
	/// Provides extensions for data types. These extensions do not make any database calls.
	/// </summary>
	public static class DataExtensions {
		
		/// <summary>
		/// Returns a Gravatar link for this account.
		/// </summary>
		public static string GetGravatarUrl(this Account Account, GravatarDefaultStyle Default) {
			string Key = Account.Email ?? Account.Username + "@codesource-email.com";
			MD5 hasher = MD5.Create ();
			byte[] Data = hasher.ComputeHash(Encoding.UTF8.GetBytes(Key.Trim ().ToLower()));
			StringBuilder Result = new StringBuilder();
			foreach(var b in Data)
				Result.Append(b.ToString("x2"));
			return "http://www.gravatar.com/avatar/" + Result.ToString().ToLower() + "?d=" + Default.ToString().ToLower();
		}
	}
}

