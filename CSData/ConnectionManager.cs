using System;

namespace CSData {
	/// <summary>
	/// Handles connections and provides easy access to DataContexts.
	/// </summary>
	public static class ConnectionManager {
		/// <summary>
		/// Creates a data context to use, with it's own connection opened.
		/// </summary>
		public static CodeSourceDataContext CreateContext() {
			return new CodeSourceDataContext("Data Source=128.233.147.140\\SQLEXPRESS;User ID=Kapps;Password=Hee1Hoo2;Initial Catalog=codesource");
		}
	}
}

