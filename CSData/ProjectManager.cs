using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;

namespace CSData {

	/// <summary>
	/// Determines how a user is using a project.
	/// </summary>
	public enum ProjectUsageType {
		/// <summary>
		/// Indicates the user is following the project.
		/// </summary>
		Following = 0,
		/// <summary>
		/// Indicates the user is a contributor to the project.
		/// </summary>
		Contributing = 1
	}
	
	/// <summary>
	/// Provides access to Project related operations.
	/// </summary>
	public class ProjectManager : AccessBase {
	
		/// <summary>
		/// Returns the project with the given ID, or null if no project had that ID.
		/// </summary>
		public Project GetProjectByID(int ID) {
			return Context.Projects.FirstOrDefault(c=>c.ID == ID);
		}
		
		/// <summary>
		/// Creates a project from a BitBucket code repository owned by RepositoryOwner and named RepositoryName.
		/// Project properties are populated from the repository.
		/// </summary>
		public Project CreateProjectFromBitbucket(Account Owner, string RepositoryOwner, string RepositoryName, Authentication Authentication) {
			BitbucketSourceControlModule Module = new BitbucketSourceControlModule(RepositoryOwner, RepositoryName, Authentication);
			var Info = Module.GetRepositoryInfo();
			Project Proj = CreateProject(Owner, RepositoryName, Info.Description, String.IsNullOrWhiteSpace(Info.Language) ? "Unknown" : Info.Language);
			using(var Access = new ModuleAccess()) {
				Access.AddModule(Proj, Module);
			}
			return Proj;
		}

		/// <summary>
		/// Gets the projects that the given user owns.
		/// </summary>
		public IEnumerable<Project> GetProjectsForUser(Account User) {
			return Context.Projects.Where (c=>c.Owner == User.ID);
		}

		/// <summary>
		/// Returns all of the projects the given user is following.
		/// </summary>
		public IEnumerable<Project> GetProjectsUserIsFollowing(Account User) {
			return Context.ProjectUsers.Where (c=>c.AccountID == User.ID).Select(c=>c.Project);
		}
	
		/// <summary>
		/// Creates a project with the given details, returning the resulting project.
		/// An exception is thrown if the project is failed to be created for any reason.
		/// </summary>
		public Project CreateProject(Account Owner, string Name, string Description, string Languages) {
			Project Proj = new Project ();
			Proj.Owner = Owner.ID;
			Proj.Description = Description;
			Proj.Name = Name;
			Proj.Languages = Languages;
			Context.Projects.InsertOnSubmit(Proj);
			SubmitChanges();
			AddUser(Proj, Owner, ProjectUsageType.Contributing);
			return Proj;
		}		
		
		/// <summary>
		/// Specifies that the given user is now using the project.
		/// Note that this method implicitly calls SubmitChanges.
		/// </summary>
		public ProjectUser AddUser(Project Proj, Account User, ProjectUsageType Type) {
			ProjectUser ProjUser = new ProjectUser ();
			ProjUser.AccountID = User.ID;
			ProjUser.ProjectID = Proj.ID;
			ProjUser.UsageType = (int)ProjectUsageType.Contributing;
			Context.ProjectUsers.InsertOnSubmit(ProjUser);
			SubmitChanges();
			return ProjUser;
		}

		/// <summary>
		/// Determines whether the given user is following the specified project.
		/// </summary>
		public bool IsUserFollowingProject(Account User, Project Project) {
			return Context.ProjectUsers.Any (c=>c.AccountID == User.ID && c.ProjectID == Project.ID);
		}

		/// <summary>
		/// Removes the given user as a follower of Project.
		/// </summary>
		public void StopFollowing (Account User, Project Project) {
			var ProjectUser = Context.ProjectUsers.FirstOrDefault(c=>c.AccountID == User.ID && c.ProjectID == Project.ID);
			if(ProjectUser == null)
				throw new ArgumentException("User is not following Project.");
			Context.ProjectUsers.DeleteOnSubmit(ProjectUser);
			Context.SubmitChanges();
		}
		
		/// <summary>
		/// Irreversibly deletes the given project and all user links for it.
		/// </summary>
		public void DeleteProject(Project Proj) {
			Context.ProjectReferences.DeleteAllOnSubmit(Context.ProjectReferences.Where (c=>c.ProjectID == Proj.ID || c.ReferencedProjectID == Proj.ID));
			Context.Projects.DeleteOnSubmit (Proj);
			SubmitChanges();
		}
		
		/// <summary>
		/// Lazily determines which projects are similar to the specified project.
		/// The results are randomized, so the same call will usually not return the exact same values for the same project.
		/// As more values are returned, future values will be exponentially more expensive to calculate.
		/// There is no guarantee as to how many results will be returned.
		/// 
		/// Or at least, that's the theory.
		/// In reality, right now it just returns the users who have the most followed projects in common
		/// selecting the project filtering where the project contains at least one of the tags of this project.
		/// In other words, a good estimation of similar projects, but no randomness involved.
		/// </summary>
		public IEnumerable<Project> GetSimilarProjects (Project Proj) {
			//HashSet<int> IncludedProjectIDs = new HashSet<int> ();
			// Essentially, for each of the users following the project, we'll take a random subsection of them.
			// Say, max(3, 10%) at a time, where they all share at least one other followed project.
			// We'll then order the amount they share descending.
			// Then, we'll return those three.
			// After this, we'll do the same thing for the tags.
			// After returning the tags, repeat.


			// Filter projects by where at least max(3, 10%) of users following are using other project.
			// If none, don't filter.
			// Then, match where at least 10% of the tags in either project are the same.
			// Then, take top 50 and return random order.
			/*var Filtered = Context.Projects.Where (c=>
				c.ProjectUsers.Select(d=>d.Account).Any (d=>c.ProjectUsers.Any (e=>e.AccountID == d.ID))
			).GroupBy(c=>c.*/

			// This is just a simple implementation until we can actually come up with a better one.
			// Just get all users following this project, find all projects they're following,
			// group by the project, filter where at least one tag and user in common, order and return.
			return Proj.ProjectUsers.Select(c=>c.Account)
				.SelectMany(c=>c.ProjectsUsing)
                .Where(c=>c.ProjectID != Proj.ID)
				.GroupBy(c=>c.Project)
				.OrderByDescending(c=>c.Count())
				//.Where (c=>c.Key.ProjectTags.Select(d=>d.Tag).Any (d=>Proj.ProjectTags.Select(e=>e.Tag).Contains (d)))
				.Where (c=>c.Any())
				.Select(c=>c.Key);
			/*Random rnd = new Random();
			while (true) {
				int Skip = rnd.Next (0, 50);
				var UsersSample = Proj.ProjectUsers.OrderByDescending(user =>
			    	user.Account.ProjectsUsing.Skip(Math.Min (Skip, user.Account.ProjectsUsing.Count - 1)).First()
				    .Project.ProjectUsers.Where (c=>Proj.ProjectUsers.Contains(c.AccountID)).Count ()
				);
				UsersSample.Take (10)
			}
			foreach (var Follower in Proj.ProjectUsers) {

			}
			// Alternate between by user and by tag.
			// Get any projects where half their tags overlap.
			return Context.Projects.OrderByDescending(c=>c.ProjectTags.Select(d=>d.Tag).Intersect(Proj.ProjectTags.Select(d=>d.Tag)).Count ())
				.Where(
					c=>c.ProjectTags.Select(d=>d.Tag).Intersect(Proj.ProjectTags.Select(d=>d.Tag)).Count() >= Proj.ProjectTags.Count / 2
					|| c.ProjectTags.Select(d=>d.Tag).Intersect(Proj.ProjectTags.Select(d=>d.Tag)).Count() >= c.ProjectTags.Count / 2
				);*/
		}


		/// <summary>
		/// Searches for projects that match the given query.
		/// The query can be a full sentence, and the results are implementation dependent.
		/// </summary>
		public IEnumerable<Project> SearchProjects (string Query) {
			// First, split it up into different terms.
			string[] Terms = Query.Split(new char[] { ',', ' ', '\t', '\n', '\r', '-' }, StringSplitOptions.RemoveEmptyEntries);
			Terms = Terms.Select(c=>RemoveNonLetters(c)).Where (c=>!String.IsNullOrWhiteSpace(c)).ToArray();
			// Then, find the projects that contain those terms. Name is worth more than description.
			// Afterwards, sort them in descending order.
			// TODO: Make this actually split the name/description and do intersect count instead of contain count, and include owner name.
            
            // Ugly code due to Linq to Sql limitations.
            // TODO: Except we actually don't bother with linq to sql for the most part, and parse everything client-side.
            // Mostly because I am not looking forward to rewriting many basic functions in sql server.
            // And mostly because in reality we'd use full text search to do all of this for us, but don't have it supported in Sql Server Express.
			var Results = Context.Projects.AsEnumerable().Select(c=> 
			    new { 
                    //Count = c.Name.Split(' ', '\t').Count(d=>Terms.Contains(d)) * 20
                      //    + c.Account.Username.Split(' ', '\t').Union(c.Account.FirstName.Split(' ', '\t')).Union(c.Account.LastName.Split(' ', '\t')).Count(d=>Terms.Contains(d)) * 5
                        //  + c.Description.Split(' ', '\t', '\r', '\n').Count(d=>Terms.Contains(d)),
        			Count = Terms.Where (d=>c.Name.ToLower().Contains(d)).Count() * 20
						  + Terms.Where (d=>c.Account.Username.ToLower().Contains(d) || c.Account.FirstName.ToLower().Contains(d) || c.Account.LastName.ToLower().Contains(d)).Count () * 5
						 + Terms.Where (d=>c.Description.ToLower().Contains(d)).Count(),
					Project = c 
				})
				.Where(c => c.Count > 0)
				.OrderByDescending(c=>c.Count)
				.Select(c=>c.Project);
			return Results;
		}

		private string RemoveNonLetters(string Input) {
            return new string(Input.Where(c => char.IsLetterOrDigit(c)).ToArray()).ToLower();
		}
		
		/// <summary>
		/// Gets all projects with the given tag.
		/// </summary>
		public IQueryable<Project> GetProjectsByTag(string TagName) {
			return Context.ProjectTags.Where (c=>c.Tag == TagName).Select(c=>c.Project);
		}
	}
}

