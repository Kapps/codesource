using System;
using System.Linq;
using System.Collections.Generic;

namespace CSData {

	/// <summary>
	/// Indicates the type of an object in Comment.ObjectType.
	/// </summary>
	public enum CommentObjectType {
		/// <summary>
		/// The comment applies to a user.
		/// </summary>
		User = 1,
		/// <summary>
		/// The comment applies to a project.
		/// </summary>
		Project = 2
	}

	/// <summary>
	/// Provides access to comments and replying.
	/// </summary>
	public class CommentManager : AccessBase {	
		/// <summary>
		/// Adds a reply with the specified contents to the given parent comment.
		/// </summary>
		public void AddReply(Comment Parent, Account User, string Contents) {
			var Comment = new Comment() {			
				OwnerID = User.ID,
				ObjectID = Parent.ObjectID,
				Contents = Contents,
				Parent = Parent.ID,
				ObjectType = Parent.ObjectType
			};
			Context.Comments.InsertOnSubmit(Comment);
			Context.SubmitChanges();
		}
		
		/// <summary>
		/// Creates a comment on the given project.
		/// </summary>
		public void CreateComment(Project Project, Account User, string Contents) {
			var Comment = new Comment() {
				OwnerID = User.ID,
				ObjectID = Project.ID,
				Contents = Contents,
				Parent = null,
                Created = DateTime.Now,
				ObjectType = (int)CommentObjectType.Project
			};
			Context.Comments.InsertOnSubmit(Comment);
			Context.SubmitChanges();
		}

		/// <summary>
		/// Creates a comment on the given project.
		/// </summary>
		public void CreateComment(Account UserToCommentOn, Account UserCommenting, string Contents) {
			var Comment = new Comment() {
				OwnerID = UserCommenting.ID,
				ObjectID = UserToCommentOn.ID,
				Contents = Contents,
				Parent = null,
				Created = DateTime.Now,
				ObjectType = (int)CommentObjectType.User
			};
			Context.Comments.InsertOnSubmit(Comment);
			Context.SubmitChanges();
		}

		/// <summary>
		/// Gets all comments posted to the given user's profile.
		/// </summary>
		public IEnumerable<Comment> GetCommentsOnUser(Account User) {
			return Context.Comments.Where(c=>c.ObjectType == (int)CommentObjectType.User && c.ObjectID == User.ID);
		}

		/// <summary>
		/// Gets all comments posted to the given project.
		/// </summary>
		public IEnumerable<Comment> GetCommentsOnProject(Project Project) {
			return Context.Comments.Where(c=>c.ObjectType == (int)CommentObjectType.Project && c.ObjectID == Project.ID);
		}
	}
}

